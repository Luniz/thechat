//
//  TCMessageCell.m
//  TheChat
//
//  Created by Chinara on 6/25/16.
//  Copyright © 2016 Chinara Kuzekeeva. All rights reserved.
//

#import "TCMessageCell.h"
#import "TCMessage.h"
#import "TCChat.h"
#import "TCUser.h"
#import "TCMedia.h"
#import "UIImageView+AsyncImageLoading.h"
#import "TCLocation.h"
#import <MapKit/MapKit.h>

static CGFloat imageViewHeight = 150.f;
static CGFloat defaultCellHeight = 50.f;

@interface TCMessageCell ()
@property (weak, nonatomic) IBOutlet UILabel *lblUserName;
@property (weak, nonatomic) IBOutlet UILabel *lblMessage;
@property (weak, nonatomic) IBOutlet UIImageView *mediaImageView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mediaImageViewHeight;
@property (nonatomic, strong) TCMessage *currentMessage;

@end
@implementation TCMessageCell


- (void) awakeFromNib {
    
    [super awakeFromNib];
    self.mediaImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.mediaImageView.clipsToBounds = YES;
    self.lblUserName.font = [UIFont systemFontOfSize:16.f
                                             weight:UIFontWeightSemibold];
    
    self.lblMessage.font = [UIFont systemFontOfSize:16.f
                                              weight:UIFontWeightRegular];
    
    self.lblMessage.textColor = [UIColor blackColor];
    self.lblMessage.numberOfLines = 0;
    self.lblUserName.textColor = [UIColor darkGrayColor];
}

- (void) layoutSubviews {
    
    if (self.currentMessage.mediaFiles.mediaThumbnailURL ||
        self.currentMessage.location.previewPathComponent) {
        self.mediaImageViewHeight.constant = imageViewHeight;
    } else {
        self.mediaImageViewHeight.constant = 0.f;
    }
    [super layoutSubviews];
}

- (void)prepareForReuse {
    [super prepareForReuse];
    self.mediaImageView.image = nil;
}

- (void) updateCellWithMessage:(TCMessage *)message {
    
    self.currentMessage = message;
    __block TCUser *user = nil;
    [message.chat.users enumerateObjectsUsingBlock:^(TCUser *obj, BOOL * _Nonnull stop) {
        if ([obj.objectId isEqual:message.userId]) {
            user = obj;
            *stop=YES;
        }
    }];
    
    self.lblUserName.text = user ? user.name : @"";
    NSString *imagePathComponent = nil;
    if (message.location) {
        self.lblMessage.text = [TCMessageCell textForLocation:message.location];
        imagePathComponent = message.location.previewPathComponent;
       
    } else {
        self.lblMessage.text = message.text;
    }
    
    if (message.mediaFiles.mediaThumbnailURL) {
        imagePathComponent = message.mediaFiles.mediaThumbnailURL;
    }
    
    [self.mediaImageView loadImageFromPath:imagePathComponent
                               placeHolder:nil];
}

+ (NSString *) textForLocation:(TCLocation *)location {
    return [NSString stringWithFormat:@"location was shared. Longitude: %@, Latitude: %@", location.longitude, location.latitude];
}

+ (NSString *) reuseIdentifier {

    static NSString *messageCellReuseId;
    if (!messageCellReuseId) {
        messageCellReuseId = NSStringFromClass([TCMessageCell class]);
    }
    return messageCellReuseId;
}

+ (CGFloat) heightForMessage:(TCMessage *)message {
   
    static NSDictionary *userNameAttributes;
    static NSDictionary *messageAttributes;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        userNameAttributes = @{NSFontAttributeName : [UIFont systemFontOfSize:16.f weight:UIFontWeightSemibold]};
        messageAttributes = @{NSFontAttributeName : [UIFont systemFontOfSize:16.f weight:UIFontWeightRegular]};
    });
    
    CGRect screenBounds = [UIScreen mainScreen].bounds;
    NSString *text = message.text.length > 0 ? message.text : [TCMessageCell textForLocation:message.location];
    CGRect messageRect = [text boundingRectWithSize:CGSizeMake(screenBounds.size.width - 20.f, FLT_MAX) options:NSStringDrawingUsesDeviceMetrics|NSStringDrawingUsesLineFragmentOrigin attributes:messageAttributes context:nil];
    CGRect userNameRect = CGRectMake(0, 0, 0, 20.f);
    
    CGFloat additionalHeight = 30.f;
    
    if (message.mediaFiles.mediaThumbnailURL ||
        message.location.previewPathComponent) {
        additionalHeight+=imageViewHeight;
    }
    
    additionalHeight = CGRectGetHeight(messageRect) + CGRectGetHeight(userNameRect) + additionalHeight;
    if (additionalHeight < defaultCellHeight) {
        return defaultCellHeight;
    }
    return ceilf(additionalHeight);

}
@end
