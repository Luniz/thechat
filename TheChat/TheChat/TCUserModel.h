//
//  TCUserModel.h
//  TheChat
//
//  Created by Chinara on 6/25/16.
//  Copyright © 2016 Chinara Kuzekeeva. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "TCUser.h"

@interface TCUserModel : NSObject

+ (instancetype) instanse;
@property (nonatomic, strong) NSNumber *currentUserId;
@property (nonatomic, strong) NSNumber *botUserId;
@end
