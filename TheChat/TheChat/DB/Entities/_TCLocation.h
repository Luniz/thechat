// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TCLocation.h instead.

#import <CoreData/CoreData.h>

extern const struct TCLocationAttributes {
	__unsafe_unretained NSString *latitude;
	__unsafe_unretained NSString *longitude;
	__unsafe_unretained NSString *previewPathComponent;
} TCLocationAttributes;

extern const struct TCLocationRelationships {
	__unsafe_unretained NSString *message;
} TCLocationRelationships;

@class TCMessage;

@interface TCLocationID : NSManagedObjectID {}
@end

@interface _TCLocation : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) TCLocationID* objectID;

@property (nonatomic, strong) NSDecimalNumber* latitude;

//- (BOOL)validateLatitude:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSDecimalNumber* longitude;

//- (BOOL)validateLongitude:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* previewPathComponent;

//- (BOOL)validatePreviewPathComponent:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) TCMessage *message;

//- (BOOL)validateMessage:(id*)value_ error:(NSError**)error_;

@end

@interface _TCLocation (CoreDataGeneratedPrimitiveAccessors)

- (NSDecimalNumber*)primitiveLatitude;
- (void)setPrimitiveLatitude:(NSDecimalNumber*)value;

- (NSDecimalNumber*)primitiveLongitude;
- (void)setPrimitiveLongitude:(NSDecimalNumber*)value;

- (NSString*)primitivePreviewPathComponent;
- (void)setPrimitivePreviewPathComponent:(NSString*)value;

- (TCMessage*)primitiveMessage;
- (void)setPrimitiveMessage:(TCMessage*)value;

@end
