// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TCUser.h instead.

#import <CoreData/CoreData.h>
#import "TCBaseObject.h"

extern const struct TCUserAttributes {
	__unsafe_unretained NSString *name;
} TCUserAttributes;

extern const struct TCUserRelationships {
	__unsafe_unretained NSString *chats;
} TCUserRelationships;

@class TCChat;

@interface TCUserID : TCBaseObjectID {}
@end

@interface _TCUser : TCBaseObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) TCUserID* objectID;

@property (nonatomic, strong) NSString* name;

//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *chats;

- (NSMutableSet*)chatsSet;

@end

@interface _TCUser (ChatsCoreDataGeneratedAccessors)
- (void)addChats:(NSSet*)value_;
- (void)removeChats:(NSSet*)value_;
- (void)addChatsObject:(TCChat*)value_;
- (void)removeChatsObject:(TCChat*)value_;

@end

@interface _TCUser (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;

- (NSMutableSet*)primitiveChats;
- (void)setPrimitiveChats:(NSMutableSet*)value;

@end
