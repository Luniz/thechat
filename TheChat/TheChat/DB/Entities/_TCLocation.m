// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TCLocation.m instead.

#import "_TCLocation.h"

const struct TCLocationAttributes TCLocationAttributes = {
	.latitude = @"latitude",
	.longitude = @"longitude",
	.previewPathComponent = @"previewPathComponent",
};

const struct TCLocationRelationships TCLocationRelationships = {
	.message = @"message",
};

@implementation TCLocationID
@end

@implementation _TCLocation

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"TCLocation" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"TCLocation";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"TCLocation" inManagedObjectContext:moc_];
}

- (TCLocationID*)objectID {
	return (TCLocationID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic latitude;

@dynamic longitude;

@dynamic previewPathComponent;

@dynamic message;

@end

