// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TCMedia.h instead.

#import <CoreData/CoreData.h>
#import "TCBaseObject.h"

extern const struct TCMediaAttributes {
	__unsafe_unretained NSString *mediaThumbnailURL;
	__unsafe_unretained NSString *mediaType;
	__unsafe_unretained NSString *mediaURL;
} TCMediaAttributes;

extern const struct TCMediaRelationships {
	__unsafe_unretained NSString *message;
} TCMediaRelationships;

@class TCMessage;

@interface TCMediaID : TCBaseObjectID {}
@end

@interface _TCMedia : TCBaseObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) TCMediaID* objectID;

@property (nonatomic, strong) NSString* mediaThumbnailURL;

//- (BOOL)validateMediaThumbnailURL:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* mediaType;

@property (atomic) int16_t mediaTypeValue;
- (int16_t)mediaTypeValue;
- (void)setMediaTypeValue:(int16_t)value_;

//- (BOOL)validateMediaType:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* mediaURL;

//- (BOOL)validateMediaURL:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) TCMessage *message;

//- (BOOL)validateMessage:(id*)value_ error:(NSError**)error_;

@end

@interface _TCMedia (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveMediaThumbnailURL;
- (void)setPrimitiveMediaThumbnailURL:(NSString*)value;

- (NSNumber*)primitiveMediaType;
- (void)setPrimitiveMediaType:(NSNumber*)value;

- (int16_t)primitiveMediaTypeValue;
- (void)setPrimitiveMediaTypeValue:(int16_t)value_;

- (NSString*)primitiveMediaURL;
- (void)setPrimitiveMediaURL:(NSString*)value;

- (TCMessage*)primitiveMessage;
- (void)setPrimitiveMessage:(TCMessage*)value;

@end
