// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TCMessage.h instead.

#import <CoreData/CoreData.h>
#import "TCBaseObject.h"

extern const struct TCMessageAttributes {
	__unsafe_unretained NSString *dateCreated;
	__unsafe_unretained NSString *dayOfDate;
	__unsafe_unretained NSString *messageType;
	__unsafe_unretained NSString *text;
	__unsafe_unretained NSString *userId;
} TCMessageAttributes;

extern const struct TCMessageRelationships {
	__unsafe_unretained NSString *chat;
	__unsafe_unretained NSString *location;
	__unsafe_unretained NSString *mediaFiles;
} TCMessageRelationships;

@class TCChat;
@class TCLocation;
@class TCMedia;

@interface TCMessageID : TCBaseObjectID {}
@end

@interface _TCMessage : TCBaseObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) TCMessageID* objectID;

@property (nonatomic, strong) NSDate* dateCreated;

//- (BOOL)validateDateCreated:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSDate* dayOfDate;

//- (BOOL)validateDayOfDate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* messageType;

@property (atomic) int16_t messageTypeValue;
- (int16_t)messageTypeValue;
- (void)setMessageTypeValue:(int16_t)value_;

//- (BOOL)validateMessageType:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* text;

//- (BOOL)validateText:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* userId;

@property (atomic) int64_t userIdValue;
- (int64_t)userIdValue;
- (void)setUserIdValue:(int64_t)value_;

//- (BOOL)validateUserId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) TCChat *chat;

//- (BOOL)validateChat:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) TCLocation *location;

//- (BOOL)validateLocation:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) TCMedia *mediaFiles;

//- (BOOL)validateMediaFiles:(id*)value_ error:(NSError**)error_;

@end

@interface _TCMessage (CoreDataGeneratedPrimitiveAccessors)

- (NSDate*)primitiveDateCreated;
- (void)setPrimitiveDateCreated:(NSDate*)value;

- (NSDate*)primitiveDayOfDate;
- (void)setPrimitiveDayOfDate:(NSDate*)value;

- (NSNumber*)primitiveMessageType;
- (void)setPrimitiveMessageType:(NSNumber*)value;

- (int16_t)primitiveMessageTypeValue;
- (void)setPrimitiveMessageTypeValue:(int16_t)value_;

- (NSString*)primitiveText;
- (void)setPrimitiveText:(NSString*)value;

- (NSNumber*)primitiveUserId;
- (void)setPrimitiveUserId:(NSNumber*)value;

- (int64_t)primitiveUserIdValue;
- (void)setPrimitiveUserIdValue:(int64_t)value_;

- (TCChat*)primitiveChat;
- (void)setPrimitiveChat:(TCChat*)value;

- (TCLocation*)primitiveLocation;
- (void)setPrimitiveLocation:(TCLocation*)value;

- (TCMedia*)primitiveMediaFiles;
- (void)setPrimitiveMediaFiles:(TCMedia*)value;

@end
