// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TCMedia.m instead.

#import "_TCMedia.h"

const struct TCMediaAttributes TCMediaAttributes = {
	.mediaThumbnailURL = @"mediaThumbnailURL",
	.mediaType = @"mediaType",
	.mediaURL = @"mediaURL",
};

const struct TCMediaRelationships TCMediaRelationships = {
	.message = @"message",
};

@implementation TCMediaID
@end

@implementation _TCMedia

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"TCMedia" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"TCMedia";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"TCMedia" inManagedObjectContext:moc_];
}

- (TCMediaID*)objectID {
	return (TCMediaID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"mediaTypeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"mediaType"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic mediaThumbnailURL;

@dynamic mediaType;

- (int16_t)mediaTypeValue {
	NSNumber *result = [self mediaType];
	return [result shortValue];
}

- (void)setMediaTypeValue:(int16_t)value_ {
	[self setMediaType:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveMediaTypeValue {
	NSNumber *result = [self primitiveMediaType];
	return [result shortValue];
}

- (void)setPrimitiveMediaTypeValue:(int16_t)value_ {
	[self setPrimitiveMediaType:[NSNumber numberWithShort:value_]];
}

@dynamic mediaURL;

@dynamic message;

@end

