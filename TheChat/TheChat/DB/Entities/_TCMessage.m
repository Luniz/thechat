// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TCMessage.m instead.

#import "_TCMessage.h"

const struct TCMessageAttributes TCMessageAttributes = {
	.dateCreated = @"dateCreated",
	.dayOfDate = @"dayOfDate",
	.messageType = @"messageType",
	.text = @"text",
	.userId = @"userId",
};

const struct TCMessageRelationships TCMessageRelationships = {
	.chat = @"chat",
	.location = @"location",
	.mediaFiles = @"mediaFiles",
};

@implementation TCMessageID
@end

@implementation _TCMessage

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"TCMessage" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"TCMessage";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"TCMessage" inManagedObjectContext:moc_];
}

- (TCMessageID*)objectID {
	return (TCMessageID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"messageTypeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"messageType"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"userIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"userId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic dateCreated;

@dynamic dayOfDate;

@dynamic messageType;

- (int16_t)messageTypeValue {
	NSNumber *result = [self messageType];
	return [result shortValue];
}

- (void)setMessageTypeValue:(int16_t)value_ {
	[self setMessageType:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveMessageTypeValue {
	NSNumber *result = [self primitiveMessageType];
	return [result shortValue];
}

- (void)setPrimitiveMessageTypeValue:(int16_t)value_ {
	[self setPrimitiveMessageType:[NSNumber numberWithShort:value_]];
}

@dynamic text;

@dynamic userId;

- (int64_t)userIdValue {
	NSNumber *result = [self userId];
	return [result longLongValue];
}

- (void)setUserIdValue:(int64_t)value_ {
	[self setUserId:[NSNumber numberWithLongLong:value_]];
}

- (int64_t)primitiveUserIdValue {
	NSNumber *result = [self primitiveUserId];
	return [result longLongValue];
}

- (void)setPrimitiveUserIdValue:(int64_t)value_ {
	[self setPrimitiveUserId:[NSNumber numberWithLongLong:value_]];
}

@dynamic chat;

@dynamic location;

@dynamic mediaFiles;

@end

