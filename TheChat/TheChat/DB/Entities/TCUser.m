#import "TCUser.h"
#import "TCMessageMapper.h"

@interface TCUser ()

// Private interface goes here.

@end

@implementation TCUser

- (NSDictionary *) dictionaryRepresenation {
    return [TCMessageMapper serializeUserEntityAttributes:self];
}
// Custom logic goes here.

@end
