// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TCBaseObject.h instead.

#import <CoreData/CoreData.h>

extern const struct TCBaseObjectAttributes {
	__unsafe_unretained NSString *objectId;
} TCBaseObjectAttributes;

@interface TCBaseObjectID : NSManagedObjectID {}
@end

@interface _TCBaseObject : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) TCBaseObjectID* objectID;

@property (nonatomic, strong) NSNumber* objectId;

@property (atomic) int64_t objectIdValue;
- (int64_t)objectIdValue;
- (void)setObjectIdValue:(int64_t)value_;

//- (BOOL)validateObjectId:(id*)value_ error:(NSError**)error_;

@end

@interface _TCBaseObject (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveObjectId;
- (void)setPrimitiveObjectId:(NSNumber*)value;

- (int64_t)primitiveObjectIdValue;
- (void)setPrimitiveObjectIdValue:(int64_t)value_;

@end
