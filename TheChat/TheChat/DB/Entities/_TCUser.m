// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TCUser.m instead.

#import "_TCUser.h"

const struct TCUserAttributes TCUserAttributes = {
	.name = @"name",
};

const struct TCUserRelationships TCUserRelationships = {
	.chats = @"chats",
};

@implementation TCUserID
@end

@implementation _TCUser

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"TCUser" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"TCUser";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"TCUser" inManagedObjectContext:moc_];
}

- (TCUserID*)objectID {
	return (TCUserID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic name;

@dynamic chats;

- (NSMutableSet*)chatsSet {
	[self willAccessValueForKey:@"chats"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"chats"];

	[self didAccessValueForKey:@"chats"];
	return result;
}

@end

