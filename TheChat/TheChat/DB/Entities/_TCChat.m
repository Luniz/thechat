// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TCChat.m instead.

#import "_TCChat.h"

const struct TCChatAttributes TCChatAttributes = {
	.name = @"name",
};

const struct TCChatRelationships TCChatRelationships = {
	.messages = @"messages",
	.users = @"users",
};

@implementation TCChatID
@end

@implementation _TCChat

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"TCChat" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"TCChat";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"TCChat" inManagedObjectContext:moc_];
}

- (TCChatID*)objectID {
	return (TCChatID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic name;

@dynamic messages;

- (NSMutableSet*)messagesSet {
	[self willAccessValueForKey:@"messages"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"messages"];

	[self didAccessValueForKey:@"messages"];
	return result;
}

@dynamic users;

- (NSMutableSet*)usersSet {
	[self willAccessValueForKey:@"users"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"users"];

	[self didAccessValueForKey:@"users"];
	return result;
}

@end

