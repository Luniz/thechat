// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TCChat.h instead.

#import <CoreData/CoreData.h>
#import "TCBaseObject.h"

extern const struct TCChatAttributes {
	__unsafe_unretained NSString *name;
} TCChatAttributes;

extern const struct TCChatRelationships {
	__unsafe_unretained NSString *messages;
	__unsafe_unretained NSString *users;
} TCChatRelationships;

@class TCMessage;
@class TCUser;

@interface TCChatID : TCBaseObjectID {}
@end

@interface _TCChat : TCBaseObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) TCChatID* objectID;

@property (nonatomic, strong) NSString* name;

//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *messages;

- (NSMutableSet*)messagesSet;

@property (nonatomic, strong) NSSet *users;

- (NSMutableSet*)usersSet;

@end

@interface _TCChat (MessagesCoreDataGeneratedAccessors)
- (void)addMessages:(NSSet*)value_;
- (void)removeMessages:(NSSet*)value_;
- (void)addMessagesObject:(TCMessage*)value_;
- (void)removeMessagesObject:(TCMessage*)value_;

@end

@interface _TCChat (UsersCoreDataGeneratedAccessors)
- (void)addUsers:(NSSet*)value_;
- (void)removeUsers:(NSSet*)value_;
- (void)addUsersObject:(TCUser*)value_;
- (void)removeUsersObject:(TCUser*)value_;

@end

@interface _TCChat (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;

- (NSMutableSet*)primitiveMessages;
- (void)setPrimitiveMessages:(NSMutableSet*)value;

- (NSMutableSet*)primitiveUsers;
- (void)setPrimitiveUsers:(NSMutableSet*)value;

@end
