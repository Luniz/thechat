//
//  TCDBConstants.h
//  TheChat
//
//  Created by Chinara on 6/25/16.
//  Copyright © 2016 Chinara Kuzekeeva. All rights reserved.
//

#ifndef TCDBConstants_h
#define TCDBConstants_h

typedef NS_ENUM(NSInteger, TCMessageType) {
    kMessageTypeNone = -1,
    kMessageTypeMedia, //
    kMessageTypeLocation,
};

typedef NS_ENUM(NSInteger, TCMediaType) {
    kMediaTypeNone = -1,
    kMediaTypePhoto, //
};

static NSUInteger const systemUserId = 98;
static NSUInteger const botUserId = 666;
static NSUInteger const systemChatId = 12;

static NSString const * messageTextKey = @"text";
static NSString const * messageLocationKey = @"location";
static NSString const * messageDateCreatedKey = @"dateCreated";
static NSString const * messageDayOfDate = @"dayOfDate";
//static NSString const * messageType = @"messageType";

static NSString const * messageMediasKey = @"mediaFiles"; // Must be dictionary
static NSString const * mediaTypeKey = @"mediaType";
static NSString const * mediaURLValueKey = @"mediaURLValue";

static NSString const * objectIdKey = @"objectId";
static NSString const * userNameKey = @"name";

static NSString const * mediaBigImageURLKey = @"mediaURL";
static NSString const * mediaThumbnailImageURLKey = @"mediaThumbnailURL";

static NSString const * locationLatitudeKey = @"latitude";
static NSString const * locationlongitudeKey = @"longitude";
static NSString const * locationPreviewKey = @"previewPathComponent";

//Relations
static NSString const * chatNameKey = @"name";
#endif




