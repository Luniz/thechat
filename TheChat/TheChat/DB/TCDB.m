//
//  TCDB.m
//  TheChat
//
//  Created by Chinara on 6/25/16.
//  Copyright © 2016 Chinara Kuzekeeva. All rights reserved.
//

#import "TCDB.h"
#import <ReactiveCocoa/ReactiveCocoa.h>

static NSString *dataModelName = @"TheChat";
static NSString *dataModelExtension = @"momd";

@interface TCDB () {
}

@property (nonatomic, strong, readwrite) NSManagedObjectModel *dataModel;
@property (nonatomic, strong, readwrite) NSManagedObjectContext *dataModelContext;
@property (nonatomic, strong, readwrite) NSManagedObjectContext *bgDataModelContext;
@property (nonatomic, strong, readwrite) NSPersistentStoreCoordinator *storeCoordinator;
@end

@implementation TCDB

- (void) addStoreWithType:(NSString *) storeType
                      URL:(NSURL *) URL {
    
    NSError *error = nil;
    
    self.storeCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:self.dataModel];
    
    [self.storeCoordinator addPersistentStoreWithType:storeType
                                    configuration:nil
                                              URL:URL
                                          options: @{NSMigratePersistentStoresAutomaticallyOption : @YES,
                                                     NSInferMappingModelAutomaticallyOption : @YES}
                                            error: &error];
    
    if (error) {
        NSAssert(NO, error.localizedDescription);
    }
}


- (NSManagedObjectContext *) dataModelContext {
    
    if (!_dataModelContext) {
        
        dispatch_sync([self defaultQueue], ^{
            
            NSPersistentStoreCoordinator *coordinator = self.storeCoordinator;
            
            if (coordinator != nil) {
                _dataModelContext = [[NSManagedObjectContext alloc]
                                          initWithConcurrencyType: NSMainQueueConcurrencyType];
                
                [_dataModelContext setPersistentStoreCoordinator: coordinator];
            }
        });
    }
    
    return _dataModelContext;
}

- (NSManagedObjectContext *) bgDataModelContext {
    
    if (!_bgDataModelContext) {
        _bgDataModelContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
        _bgDataModelContext.parentContext = self.dataModelContext;
    }
    
    return _bgDataModelContext;
}


- (NSManagedObjectModel *) dataModel {
    
    
    if (!_dataModel) {
        
        dispatch_sync([self defaultQueue], ^{
            
            NSURL *URL = [[NSBundle mainBundle] URLForResource:dataModelName
                                                 withExtension:dataModelExtension];
            
            _dataModel = [[NSManagedObjectModel alloc] initWithContentsOfURL: URL];
        });
    }
    
    return _dataModel;
}

#pragma mark - Utils

- (dispatch_queue_t) defaultQueue {
    
    static dispatch_queue_t queue;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        queue = dispatch_queue_create("DataBaseQueue", DISPATCH_QUEUE_CONCURRENT);
    });
    
    return queue;
}
@end
