//
//  TCDB.h
//  TheChat
//
//  Created by Chinara on 6/25/16.
//  Copyright © 2016 Chinara Kuzekeeva. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface TCDB : NSObject

@property (nonatomic, strong, readonly) NSManagedObjectModel *dataModel;
@property (nonatomic, strong, readonly) NSManagedObjectContext *dataModelContext;
@property (nonatomic, strong, readonly) NSPersistentStoreCoordinator *storeCoordinator;
@property (nonatomic, strong, readonly) NSManagedObjectContext *bgDataModelContext;

- (void) addStoreWithType:(NSString *) storeType
                      URL:(NSURL *) URL;
- (dispatch_queue_t) defaultQueue;
@end
