//
//  TCDBWrapper.h
//  TheChat
//
//  Created by Chinara on 6/25/16.
//  Copyright © 2016 Chinara Kuzekeeva. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import <ReactiveCocoa/ReactiveCocoa.h>

@class TCChat;
@class TCMessage;
@class TCUser;

@interface TCDBWrapper : NSObject

+ (instancetype) instance;

- (NSFetchedResultsController *) messagesForChatId:(NSNumber*)chatId;

- (void) createMessageWithDictionary:(NSDictionary *)dictionary;

- (TCUser *) userWithDictionary:(NSDictionary *)dictionary;
- (TCUser *) userWithId:(NSNumber*)userId;
- (TCChat *) chatWithId:(NSUInteger)chatId;

- (RACSignal *) chatWithDictionary:(NSDictionary *)dictionary;

- (void) save;
@end
