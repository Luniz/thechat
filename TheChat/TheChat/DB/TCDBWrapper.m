
//
//  TCDBWrapper.m
//  TheChat
//
//  Created by Chinara on 6/25/16.
//  Copyright © 2016 Chinara Kuzekeeva. All rights reserved.
//

#import "TCDBWrapper.h"
#import "TCChat.h"
#import "TCMessage.h"
#import "TCDB.h"
#import "TCMessageMapper.h"
#import "TCUser.h"
#import "TCDBConstants.h"
#import "NSFileManager+TheChat.h"

@interface TCDBWrapper ()

@property (nonatomic, strong) TCDB *database;
@property (nonatomic, strong) TCMessageMapper *messageMapper;
@end

@implementation TCDBWrapper

+ (instancetype) instance {

    static TCDBWrapper *wrapper;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        
        wrapper = [[TCDBWrapper alloc] init];
        wrapper.messageMapper = [TCMessageMapper new];
        
        wrapper.database = [[TCDB alloc] init];
        
        NSURL *url = [NSFileManager libraryDirectoryURL];
        
        NSAssert(url != nil, @"NSURL class expected");
        
        url = [url URLByAppendingPathComponent:@"messages.sql"
                                   isDirectory:NO];
        
        [wrapper.database addStoreWithType:NSSQLiteStoreType
                                       URL:url];
        
    });
    return wrapper;
}

- (NSFetchedResultsController *) messagesForChatId:(NSNumber *)chatId {
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:[TCMessage entityName]];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.chat.objectId == %@", chatId];
    
    NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"dateCreated" ascending:YES];
    
    request.predicate = predicate;
    request.sortDescriptors = @[descriptor];
    
     NSFetchedResultsController *controller = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:self.database.dataModelContext sectionNameKeyPath:@"dayOfDate" cacheName:nil];
    [controller performFetch:nil];
    return controller;
}


- (NSDictionary *) insertObjectToDictionary:(NSDictionary*)dictionary
                                  forEntity:(NSString *)entityName {
 
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:entityName];
    
    NSAssert(request != nil, @"NSFetchRequest obejct can't be nil.");
    
    NSUInteger objectId = [self objectIdForRequest:request];
    NSMutableDictionary *extendedDictionary = dictionary.mutableCopy;
    extendedDictionary[objectIdKey] = @(objectId);
    return extendedDictionary.copy;
}

- (void) createMessageWithDictionary:(NSDictionary *)dictionary {
    
    __block NSDictionary *extendedDictionary = [self insertObjectToDictionary:dictionary forEntity:[TCMessage entityName]];
    
    @weakify(self);
    [self.database.bgDataModelContext performBlock:^{
        @strongify(self);
      __unused TCMessage *message =  [self.messageMapper messageFromDictionary:extendedDictionary
                                      withContext:self.database.bgDataModelContext];
        TCUser *user = [self userWithId:message.userId];
        [message.chat addUsersObject:user];
        [self.database.bgDataModelContext save:nil];
    }];
}

- (TCUser *) userWithDictionary:(NSDictionary *)dictionary {
    
    __block NSDictionary *extendedDictionary = nil;
    __block TCUser *user = nil;
    
    if (!dictionary[objectIdKey]) {
        
        extendedDictionary = [self insertObjectToDictionary:dictionary
                                                  forEntity:[TCUser entityName]];
        
    } else {
        
        user = [self userWithId:dictionary[objectIdKey]];
        if (user) {
            return user;
        }
        extendedDictionary = dictionary;
    }
    
    @weakify(self);

    [self.database.dataModelContext performBlockAndWait:^{
    
        @strongify(self);
        user = [self.messageMapper userFromDictionary:extendedDictionary 
                                              withContext:self.database.dataModelContext];
        
        [self.database.dataModelContext save:nil];
    }];
    
    return user;
}

- (TCUser *) userWithId:(NSNumber *)userId {
    
    __block NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:[TCUser entityName]];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.objectId == %@", userId];
    
    request.predicate = predicate;
    
    __block TCUser *user = nil;
    
    @weakify(self);
    [self.database.dataModelContext performBlockAndWait:^{
        @strongify(self);
        NSArray *fetched =
        [self.database.dataModelContext executeFetchRequest:request
                                                      error:nil];
        user = fetched.firstObject;
    }];
    return user;
}

- (TCChat *) chatWithId:(NSUInteger)chatId {
    
    __block NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:[TCChat entityName]];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.objectId == %@", @(chatId)];
    
    request.predicate = predicate;
    
    __block TCChat *chat = nil;
    
    @weakify(self);
    [self.database.dataModelContext performBlockAndWait:^{
        @strongify(self);
        NSArray *fetched =
        [self.database.dataModelContext executeFetchRequest:request
                                                      error:nil];
        chat = fetched.firstObject;
    }];
    
    return chat;
}

- (RACSignal *) chatWithDictionary:(NSDictionary *)dictionary {
    
    @weakify(self);
    
    __block NSDictionary *newDictionary = dictionary;
    
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        @strongify(self);
        
        [self.database.bgDataModelContext performBlock:^{
            
            @strongify(self);
            TCChat *chat = [self.messageMapper chatFromDictionary:newDictionary
                                                      withContext:self.database.bgDataModelContext];
            
            [self.database.bgDataModelContext save:nil];
            [subscriber sendNext:chat];
            [subscriber sendCompleted];
        }];
        
        return nil;
    }];
}

#pragma mark - Utils 

- (NSInteger) objectIdForRequest:(NSFetchRequest *) request {
    
    NSFetchRequest *newRequest = [request copy];
    
    [newRequest setResultType:NSManagedObjectResultType];
    
    [newRequest setPropertiesToFetch:[NSArray arrayWithObjects:@"objectId", nil]];
    
    __block NSInteger index = 0;
    
    [self.database.bgDataModelContext performBlockAndWait:^{
        
        NSArray<NSNumber*> *items = [[self.database.bgDataModelContext executeFetchRequest:newRequest error:nil] valueForKey:@"objectId"];
        
        items = [items sortedArrayUsingSelector:@selector(compare:)];
        
        NSUInteger maxValue = items.lastObject.integerValue;
        
        if (items.count == maxValue) {
            index = maxValue+1;
        } else {
            
            for (NSUInteger i = 0; i < items.count-1; i++) {
                if (i - items[i].integerValue != 0) {
                    index = i;
                    break;
                }
            }
        }
    }];
    
    return index;
}

- (void) save {
    [self.database.dataModelContext save:nil];
}

@end
