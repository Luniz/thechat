//
//  TCLocationToDataTransformer.m
//  TheChat
//
//  Created by Chinara on 6/25/16.
//  Copyright (c) 2014 Kuzekeeva Chinara. All rights reserved.
//

#import "TCLocationToDataTransformer.h"
#import <MapKit/MapKit.h>

@implementation TCLocationToDataTransformer

+ (BOOL)allowsReverseTransformation {
    return YES;
}

+ (Class)transformedValueClass {
    return [NSData class];
}


- (id)transformedValue:(id)value {
    
    NSAssert([value isKindOfClass:[CLLocation class]], @"CLLocation class expected.");
    
    CLLocation *location = (CLLocation *)value;
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:location];
    return data;
}

- (id)reverseTransformedValue:(id)value {
    
    NSAssert([value isKindOfClass:[NSData class]], @"NSData class expected.");
    
    NSData *data = (NSData *)value;
    CLLocation *location = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    return location;
}

@end
