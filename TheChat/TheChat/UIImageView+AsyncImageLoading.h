//
//  UIImageView+AsyncImageLoading.h
//  TheChat
//
//  Created by Chinara on 6/26/16.
//  Copyright © 2016 Chinara Kuzekeeva. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (AsyncImageLoading)

- (void) loadImageFromPath:(NSString *)path
               placeHolder:(UIImage *)placeholder;
@end
