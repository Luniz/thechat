//
//  TCChatModelProtocol.h
//  TheChat
//
//  Created by Chinara on 6/25/16.
//  Copyright © 2016 Chinara Kuzekeeva. All rights reserved.
//


@class NSFetchedResultsController;
@class RACSignal;
@class Foundation;
@class UIImage;
@class CLLocation;
@class TCMessage;
@class TCChat;

@protocol TCChatModelProtocol <NSObject>

@property (nonatomic, strong, readonly) NSFetchedResultsController *messagesController;
@property (nonatomic, strong) RACSignal *messageInsertSignal;
@property (nonatomic, strong) RACSignal *messageDeleteSignal;
@property (nonatomic, strong) RACSignal *messageUpdateSignal;

+ (instancetype) modelWithChatId:(NSUInteger)chatId;

- (RACSignal *) loadAllMessagesForChat;

- (void) deleteMessage:(TCMessage *)message;

- (void) sendTextMessage:(NSString *)messageText;

- (void) sendImageMessage:(UIImage *)image
                  withURL:(NSURL *)assetURL;

- (void) sendMessageWithLatitude:(NSNumber *)latitude 
                       longitude:(NSNumber *)longitude
                 andPreviewImage:(UIImage *)image;
@end
