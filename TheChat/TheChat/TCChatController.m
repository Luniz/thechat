//
//  TCChatController.m
//  TheChat
//
//  Created by Chinara on 6/25/16.
//  Copyright © 2016 Chinara Kuzekeeva. All rights reserved.
//

#import "TCChatController.h"
#import "TCInputView.h"
#import "TCMessageCell.h"
#import "TCMessage.h"
#import "TCUser.h"
#import "TCChat.h"
#import "TCMapController.h"

#import <CoreData/CoreData.h>
#import <MobileCoreServices/MobileCoreServices.h>

@interface TCChatController () <UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *inputBottomSpace;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *inputContainerView;

@property (nonatomic, strong) TCInputView *inputView;
@property (nonatomic) BOOL isAppearedFirstTime;

///// Keyboard animator
@property (nonatomic) BOOL isViewRotating;
@property (nonatomic) CGFloat animationDuration;
@property (nonatomic) NSUInteger animationCurve;
@end

@implementation TCChatController

#pragma mark - Lifecircle

- (instancetype) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    return self;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    self.isAppearedFirstTime = YES;
    [self registerTableViewCells];
    self.inputView = [[TCInputView alloc] initWithFrame:self.inputContainerView.bounds];
    [self.inputContainerView addSubview:self.inputView];
    
    NSDictionary *views = @{@"inputView" : self.inputView};
    
    self.inputView.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray *horisontal = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[inputView]-|" options:0 metrics:nil views:views];
    NSArray *vertical = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[inputView]-|" options:0 metrics:nil views:views];
    [self.inputContainerView addConstraints:horisontal];
    [self.inputContainerView addConstraints:vertical];
    
    [self bindGestures];
    [self bindActions];
}


- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    NSAssert((self.model != nil), @"Model can't be NIL");
    
    [self subscribeToKeyboardNotifications];
    
    @weakify(self);
    [[[self.model loadAllMessagesForChat] deliverOnMainThread] subscribeNext:^(id x) {
        @strongify(self);
        [self.tableView reloadData];
    }];
}

- (void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self unsubscribeFromKeyboardNotifications];
}

#pragma mark - Memory

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) dealloc {
    NSLog(@"dealloc");
}

#pragma mark - UI

- (void) registerTableViewCells {

//    [self.tableView registerClass:[TCMessageCell class]
//           forCellReuseIdentifier:[TCMessageCell reuseIdentifier]];
}

#pragma mark - Actions

- (void) unsubscribeFromKeyboardNotifications {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillChangeFrameNotification object:nil];
}

- (void) subscribeToKeyboardNotifications {

    @weakify(self);
    [[[[NSNotificationCenter defaultCenter] rac_addObserverForName:UIKeyboardWillChangeFrameNotification object:nil] deliverOnMainThread] subscribeNext:^(NSNotification *x) {
        
        @strongify(self);
        NSDictionary *userInfo = x.userInfo;
        
        CGRect finalFrame = [userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
        
        finalFrame = [self.view convertRect:finalFrame
                                   fromView:self.view.window];
        
        if (self.isViewRotating) {
            
            [self adjustInputFieldWithKeyboardFrame:finalFrame
                                  animationDuration:self.animationDuration
                                  andAnimationCurve:self.animationCurve];
            
        } else {
            
            
            CGFloat duration = [userInfo[UIKeyboardAnimationDurationUserInfoKey] floatValue];
            
            if (duration > 0) {
                
                NSUInteger curve = [userInfo[UIKeyboardAnimationCurveUserInfoKey] integerValue];
                [self adjustInputFieldWithKeyboardFrame:finalFrame
                                      animationDuration:duration
                                      andAnimationCurve:curve];
            }
        }
    }];
}

- (void) bindActions {
    
    @weakify(self);
    [self.inputView.attachmentSignal subscribeNext:^(id x) {
        @strongify(self);
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Choose action" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
        UIAlertAction *mapAction = [UIAlertAction actionWithTitle:@"Send location" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self performSegueWithIdentifier:@"showMapController" sender:nil];
        }];
        
        UIAlertAction *imagePickerACtion = [UIAlertAction actionWithTitle:@"Send image" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            void(^showImagePickerControlle)() = ^void() {
                UIImagePickerController *controller = [[UIImagePickerController alloc] init];
                controller.mediaTypes = @[(__bridge NSString *)kUTTypeImage];
                controller.delegate = self;
                [self presentViewController:controller animated:YES completion:nil];
            };
            
            if ([self.inputView isFirstResponder]) {
                [self.inputView resignFirstResponder];
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    showImagePickerControlle();
                });
            } else {
                showImagePickerControlle();
            }
        }];
        
        [alert addAction:cancelAction];
        [alert addAction:imagePickerACtion];
        [alert addAction:mapAction];
        [self presentViewController:alert animated:YES completion:nil];
    }];
    
    [self.inputView.sendSignal subscribeNext:^(id x) {
        
        @strongify(self);
        
        NSString *text = self.inputView.messageText;
        if (text.length == 0) {
            return;
        }
        [self.model sendTextMessage:text];
        [self.inputView clearInputField];
    }];
    
    [[self.model.messageInsertSignal deliverOnMainThread] subscribeNext:^(NSIndexPath *x) {
        @strongify(self);
        
        if (x.row == 0) {
            [self.tableView reloadData];
        } else {
            [self.tableView beginUpdates];
            
            [self.tableView insertRowsAtIndexPaths:@[x]
                                  withRowAnimation:UITableViewRowAnimationNone];
            [self.tableView endUpdates];
        }
        
        [self.tableView scrollToRowAtIndexPath:x atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    }];
}

- (void) bindGestures {

    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:nil action:nil];
    
    tap.delegate = self;
    
    [self.tableView addGestureRecognizer:tap];
    
    @weakify(self);
    [[tap rac_gestureSignal] subscribeNext:^(id x) {
        @strongify(self);
        [self.inputView resignFirstResponder];
    }];
}

#pragma mark - Keyboard

- (void) adjustInputFieldWithKeyboardFrame:(CGRect)keyboardFrame  {
    
    self.inputBottomSpace.constant = self.view.bounds.size.height - keyboardFrame.origin.y;
    [self.view layoutSubviews];
}


- (void) adjustInputFieldWithKeyboardFrame:(CGRect)frame
                         animationDuration:(CGFloat)duration
                         andAnimationCurve:(NSUInteger)curve {

    [UIView animateWithDuration:duration
                          delay:0.
                        options:curve
                     animations:^{
                         [self adjustInputFieldWithKeyboardFrame:frame];
                     } completion:nil];
}

#pragma mark - Rotation

- (void) willTransitionToTraitCollection:(UITraitCollection *)newCollection withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    
    [super willTransitionToTraitCollection:newCollection
                 withTransitionCoordinator:coordinator];

    [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context) {
       
        self.animationDuration = [context transitionDuration];
        self.animationCurve = [context completionCurve];
        self.isViewRotating = YES;

    } completion:^(id<UIViewControllerTransitionCoordinatorContext>  _Nonnull context) {
        self.isViewRotating = NO;
    }];
}

#pragma mark - UITableViewDelegate

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self.inputView resignFirstResponder];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    TCMessage *message = [self.model.messagesController objectAtIndexPath:indexPath];
    return [TCMessageCell heightForMessage:message];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 20.f;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    NSArray<id<NSFetchedResultsSectionInfo>> *sections = self.model.messagesController.sections;
    
    id<NSFetchedResultsSectionInfo> sectionInfo = sections[section];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(tableView.bounds), 20)];
    label.textAlignment = NSTextAlignmentCenter;
    label.text = sectionInfo.name;
    label.backgroundColor = [UIColor blackColor];
    label.textColor = [UIColor whiteColor];
    return label;
}

- (void)tableView:(UITableView *)tableView
  willDisplayCell:(UITableViewCell *)cell
forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (self.isAppearedFirstTime) {
        
        NSArray<id<NSFetchedResultsSectionInfo>> *sections = self.model.messagesController.sections;
        
        id<NSFetchedResultsSectionInfo> sectionInfo = sections.lastObject;
        
        NSIndexPath *iPath = [NSIndexPath indexPathForItem:sectionInfo.numberOfObjects-1 inSection:sections.count-1];
        
        [self.tableView scrollToRowAtIndexPath:iPath
                              atScrollPosition:UITableViewScrollPositionBottom
                                      animated:NO];
        
        self.isAppearedFirstTime = NO;
        
    }
    
    TCMessageCell *messageCell = (TCMessageCell *)cell;
    TCMessage *message = [self.model.messagesController objectAtIndexPath:indexPath];
    [messageCell updateCellWithMessage:message];
    
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    NSArray *sections = self.model.messagesController.sections;
    return sections.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self numberOfRowsInSection:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    TCMessageCell *cell = [tableView dequeueReusableCellWithIdentifier:[TCMessageCell reuseIdentifier] forIndexPath:indexPath];
    return cell;
}


#pragma mark UIGestureRecognizerDelegate methods

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    
    CGPoint point = [touch locationInView:self.tableView];
    
    if ([self.tableView indexPathForRowAtPoint:point]) {
        return NO;
    }
    
    return YES;
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    
    UIImage *selectedImage = [info objectForKey:UIImagePickerControllerOriginalImage];
    NSURL *assetURL = info[UIImagePickerControllerReferenceURL];
    
    [self.model sendImageMessage:selectedImage
                         withURL:assetURL];
    
    [picker dismissViewControllerAnimated:YES
                               completion:nil];
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

    if ([segue.identifier isEqualToString:@"showMapController"]) {
        UINavigationController *navigationController = segue.destinationViewController;
        TCMapController *ctrl = navigationController.viewControllers.firstObject;
        if ([ctrl respondsToSelector:@selector(locationSelectionSignal)]) {
            @weakify(self);
            [ctrl.locationSelectionSignal subscribeNext:^(RACTuple *tuple) {
                @strongify(self);
                if (tuple != nil) {
                    UIImage *image = tuple.third;
                    NSNumber *latitude = [tuple.first copy];
                    NSNumber *longitude = [tuple.second copy];
                    [self.model sendMessageWithLatitude:latitude
                                              longitude:longitude
                                        andPreviewImage:image];
                }
            }];
        }
    }
    
}

#pragma mark - Utils

- (NSUInteger) numberOfRowsInSection:(NSUInteger)section {
    NSArray<id<NSFetchedResultsSectionInfo>> *sections = self.model.messagesController.sections;
    id<NSFetchedResultsSectionInfo> sectionInfo = sections[section];
    NSUInteger count = sectionInfo.numberOfObjects;
    return count;
}
@end
