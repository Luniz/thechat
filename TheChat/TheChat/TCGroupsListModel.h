//
//  TCGroupsListModel.h
//  TheChat
//
//  Created by Chinara on 6/26/16.
//  Copyright © 2016 Chinara Kuzekeeva. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ReactiveCocoa/ReactiveCocoa.h>

#import "TCChat.h"

@interface TCGroupsListModel : NSObject

@property (nonatomic) NSUInteger chatId;
@property (nonatomic, strong) NSString *chatName;

- (RACSignal *) createDefaultChatEntity;
@end
