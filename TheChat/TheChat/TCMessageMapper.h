//
//  TCMessageMapper.h
//  TheChat
//
//  Created by Chinara on 6/25/16.
//  Copyright © 2016 Chinara Kuzekeeva. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class TCMessage;
@class TCUser;
@class TCChat;

@interface TCMessageMapper : NSObject

- (TCMessage *) messageFromDictionary:(NSDictionary *)dictionary
                          withContext:(NSManagedObjectContext *)context;

- (TCUser *) userFromDictionary:(NSDictionary *)dictionary
                    withContext:(NSManagedObjectContext *)context;

- (TCChat *) chatFromDictionary:(NSDictionary *)dictionary
                    withContext:(NSManagedObjectContext *)context;

+ (NSDictionary *) serializeEntity:(id)managedObject
                          withName:(NSString *)name
                         inContext:(NSManagedObjectContext *)context;

+ (NSDictionary *) serializeUserEntityAttributes:(TCUser *)user;

@end
