//
//  TCChatController.h
//  TheChat
//
//  Created by Chinara on 6/25/16.
//  Copyright © 2016 Chinara Kuzekeeva. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TCChatModelProtocol.h"

@interface TCChatController : UIViewController

@property (nonatomic, strong) NSObject<TCChatModelProtocol>* model;


@end

