//
//  TCGroupsListModel.m
//  TheChat
//
//  Created by Chinara on 6/26/16.
//  Copyright © 2016 Chinara Kuzekeeva. All rights reserved.
//

#import "TCGroupsListModel.h"
#import "TCDBWrapper.h"
#import "TCUserModel.h"
#import "TCDBConstants.h"
#import "TCChat.h"

@implementation TCGroupsListModel

- (RACSignal *) createDefaultChatEntity {
    
    @weakify(self);
    NSString *chatName = @"Temporary Madness";
    
    TCUser *user = [[TCDBWrapper instance] userWithId:[TCUserModel instanse].currentUserId];
    NSDictionary *userDictionary = user.dictionaryRepresenation;
    
    return [[[[TCDBWrapper instance] chatWithDictionary:@{objectIdKey : @(systemChatId), chatNameKey : chatName, @"user" : userDictionary}] doNext:^(TCChat *x) {
        @strongify(self);
        self.chatId = x.objectId.integerValue;
        self.chatName = x.name;
    }] deliverOnMainThread];
}
@end
