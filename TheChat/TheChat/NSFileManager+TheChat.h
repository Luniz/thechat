//
//  NSFileManager+TCFileManager.h
//  TheChat
//
//  Created by Chinara on 6/26/16.
//  Copyright © 2016 Chinara Kuzekeeva. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSFileManager (TheChat)

+ (NSURL *) libraryDirectoryURL;
@end
