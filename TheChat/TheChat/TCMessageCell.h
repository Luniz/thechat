//
//  TCMessageCell.h
//  TheChat
//
//  Created by Chinara on 6/25/16.
//  Copyright © 2016 Chinara Kuzekeeva. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TCMessage;

@interface TCMessageCell : UITableViewCell

+ (NSString *) reuseIdentifier;

- (void) updateCellWithMessage:(TCMessage *)message;
+ (CGFloat) heightForMessage:(TCMessage *)message;
@end
