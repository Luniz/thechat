//
//  TCMapController.m
//  TheChat
//
//  Created by Chinara on 6/27/16.
//  Copyright © 2016 Chinara Kuzekeeva. All rights reserved.
//

#import "TCMapController.h"
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import <ReactiveCocoa/ReactiveCocoa.h>

@interface TCMapController () <MKMapViewDelegate, CLLocationManagerDelegate>

@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *actIndicator;
@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic) BOOL isLocationUpdated;
@end

@implementation TCMapController

- (instancetype) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self bindActions];
    }
    return self;
}

- (void) viewDidLoad {
    [super viewDidLoad];
    
    CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
    if (status != kCLAuthorizationStatusAuthorizedWhenInUse) {
        self.locationManager = [[CLLocationManager alloc] init];
        self.locationManager.delegate = self;
        [self.locationManager requestWhenInUseAuthorization];
    } else {
        [self beginMapSession];
    }
}

- (void) bindActions {
    self.locationSelectionSignal = [RACBehaviorSubject behaviorSubjectWithDefaultValue:nil];
}

- (void) adjustNavigationBar {
    
    RACSignal *annotationsCountSignal = [[RACObserve(self, mapView.annotations) takeUntil:self.rac_willDeallocSignal] map:^id(NSArray *value) {
        return @(value.count > 0);
    }];
    
    @weakify(self);
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Share" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:nil action:nil];
    
     self.navigationItem.leftBarButtonItem.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
         @strongify(self);
         dispatch_async(dispatch_get_main_queue(), ^{
             [self dismissViewControllerAnimated:YES
                                      completion:nil];
         });
         return [RACSignal empty];
     }];
    self.navigationItem.rightBarButtonItem.rac_command = [[RACCommand alloc] initWithEnabled:annotationsCountSignal signalBlock:^RACSignal *(id input) {
        @strongify(self);
        
        [self.actIndicator startAnimating];
        
        [[self getLocationScreenShot] subscribeNext:^(RACTuple *tuple) {
            @strongify(self);
            
            [self.locationSelectionSignal sendNext:tuple];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.actIndicator stopAnimating];
            });
            
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self dismissViewControllerAnimated:YES
                                         completion:nil];
            });
            
        } error:^(NSError *error) {
            @strongify(self);
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:error.localizedDescription preferredStyle:UIAlertControllerStyleAlert];
                [self presentViewController:alert animated:YES completion:nil];
            });
        
        }];
        return [RACSignal empty];
    }];
}

- (void) beginMapSession {
    
    [self adjustNavigationBar];
    self.mapView.showsUserLocation = YES;
    
    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:nil action:nil];
    @weakify(self);
    [[longPress rac_gestureSignal] subscribeNext:^(UILongPressGestureRecognizer *x) {
        @strongify(self);
        CGPoint touchLocation = [x locationInView:self.mapView];
        CLLocationCoordinate2D locaton = [self.mapView convertPoint:touchLocation toCoordinateFromView:self.mapView];
        MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
        annotation.coordinate = locaton;
        
        [self.mapView removeAnnotations:self.mapView.annotations];
        
        [self.mapView addAnnotation:annotation];
        
    }];
    
    [self.mapView addGestureRecognizer:longPress];
}

- (RACSignal *) getLocationScreenShot {

    if (![MKMapSnapshotOptions class]) {
        NSError *error = [NSError errorWithDomain:@"iOSVersion" code:0 userInfo:@{NSLocalizedDescriptionKey : @"iOS version must be 9.0 at least"}];
        return [RACSignal return:error];
    }
    
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        
        __block NSObject<MKAnnotation> *annotation = self.mapView.annotations.firstObject;
        MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(annotation.coordinate, 1000, 1000);
        
        MKMapSnapshotOptions *options = [[MKMapSnapshotOptions alloc] init];
        options.region = region;
        
        options.scale = [UIScreen mainScreen].scale;
        options.size = CGSizeMake(300, 200);
        
        MKMapSnapshotter *snapshotter = [[MKMapSnapshotter alloc] initWithOptions:options];
        [snapshotter startWithCompletionHandler:^(MKMapSnapshot *snapshot, NSError *error) {
            
            if (error) {
                [subscriber sendError:error];
            } else {
             
                UIImage *image = snapshot.image;
                
                MKAnnotationView *annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"mkview"];
                UIImage *pinImage = [UIImage imageNamed:@"pin"];
                annotationView.image = pinImage;
                
                @autoreleasepool {
                    UIGraphicsBeginImageContextWithOptions(image.size, true, image.scale);
                    CGPoint point = [snapshot pointForCoordinate:annotation.coordinate];
                    
                    [image drawAtPoint:CGPointMake(0, 0)];
                    
                    [annotationView drawViewHierarchyInRect:CGRectMake(point.x - pinImage.size.width/2.f, point.y-pinImage.size.height, annotationView.frame.size.width, annotationView.frame.size.height) afterScreenUpdates: true];
                    
                    UIImage *finalImage = UIGraphicsGetImageFromCurrentImageContext();
                    UIGraphicsEndImageContext();
 
                    [subscriber sendNext:RACTuplePack(@(annotation.coordinate.latitude), @(annotation.coordinate.longitude), finalImage)];
                    [subscriber sendCompleted];
                }
            }
        }];
        return nil;
    }];
    
}

#pragma mark - MKMapViewDelegate

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation {
    
    if (!self.isLocationUpdated) {
        self.isLocationUpdated = YES;
        MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(userLocation.coordinate, 1000, 1000);
        MKCoordinateRegion newRegion = [self.mapView regionThatFits:region];
        [self.mapView setRegion:newRegion animated:YES];
    }
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    
    if (status == kCLAuthorizationStatusAuthorizedWhenInUse) {
        [self beginMapSession];
    }
}

@end
