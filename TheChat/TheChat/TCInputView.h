//
//  TCInputView.h
//  TheChat
//
//  Created by Chinara on 6/25/16.
//  Copyright © 2016 Chinara Kuzekeeva. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ReactiveCocoa/ReactiveCocoa.h>

@interface TCInputView : UIView

@property (nonatomic, strong, readonly) RACSignal *attachmentSignal;
@property (nonatomic, strong, readonly) RACSignal *sendSignal;
@property (nonatomic, strong, readonly) NSString *messageText;
- (void) clearInputField;
@end
