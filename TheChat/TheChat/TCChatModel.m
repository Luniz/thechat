//
//  TCChatModel.m
//  TheChat
//
//  Created by Chinara on 6/25/16.
//  Copyright © 2016 Chinara Kuzekeeva. All rights reserved.
//

#import "TCChatModel.h"
#import <MapKit/MapKit.h>
#import "TCChat.h"
#import "TCMessage.h"
#import "TCDBWrapper.h"
#import "TCDBConstants.h"
#import "TCUserModel.h"
#import "TCMessageSender.h"

#import "NSFileManager+TheChat.h"
#import <ImageIO/ImageIO.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <ReactiveCocoa/ReactiveCocoa.h>


static NSString* const thumbnailURLSuffixKey = @"thumbnail";

@interface TCChatModel () <NSFetchedResultsControllerDelegate>

@property (nonatomic, strong) NSNumber *currentChatId;
@property (nonatomic, strong) TCDBWrapper *dbWrapper;

@property (nonatomic, strong, readwrite) NSFetchedResultsController *messagesController;
@property (nonatomic, strong, readwrite) RACSignal *messageUpdated;
@property (nonatomic, strong) RACSignal * fetchedResultsControllerSignal;
@property (nonatomic, strong) TCMessageSender *messageSender;
@end

@implementation TCChatModel
@synthesize messagesController = _messagesController;
@synthesize messageInsertSignal = _messageInsertSignal;
@synthesize messageDeleteSignal = _messageDeleteSignal;
@synthesize messageUpdateSignal = _messageUpdateSignal;

+ (instancetype) modelWithChatId:(NSUInteger)chatId {
    
    TCChatModel *model = [[TCChatModel alloc] init];
    model.dbWrapper = [TCDBWrapper new];
    model.currentChatId = [NSNumber numberWithInteger:chatId];
    model.messageSender = [TCMessageSender new];
    [model bindActions];
    return model;
}

- (RACSignal *) loadAllMessagesForChat {
    
    @weakify(self);
    
    return [[RACSignal startLazilyWithScheduler:[RACScheduler schedulerWithPriority:RACSchedulerPriorityBackground] block:^(id<RACSubscriber> subscriber) {
        
        @strongify(self);
        if (!self.messagesController) {
            self.messagesController = [[TCDBWrapper instance] messagesForChatId:self.currentChatId];
            self.messagesController.delegate = self;
        }
        
        [subscriber sendNext:nil];
        [subscriber sendCompleted];
        
    }] deliverOnMainThread];
}

- (void) unbindActions {
    
}

- (NSIndexPath *) indexPathFromTuple:(RACTuple *)tuple {
    
    NSIndexPath *iPath = tuple.fifth;
    NSAssert([iPath isKindOfClass:[NSIndexPath class]], @"NSIndexPath class expected");
    return iPath;
}
- (void) bindActions {
    
    self.fetchedResultsControllerSignal = [[self rac_signalForSelector:@selector(controller:didChangeObject:atIndexPath:forChangeType:newIndexPath:) fromProtocol:@protocol( NSFetchedResultsControllerDelegate)] takeUntil:self.rac_willDeallocSignal];
    
    @weakify(self);
    self.messageUpdateSignal = [[self.fetchedResultsControllerSignal filter:^BOOL(RACTuple *value) {
        NSNumber *changeTypeValue = value.fourth;
        return changeTypeValue.integerValue == NSFetchedResultsChangeUpdate;
    }] map:^id(RACTuple *value) {
        @strongify(self);
        return [self indexPathFromTuple:value];
    }];
    
    self.messageInsertSignal = [[self.fetchedResultsControllerSignal filter:^BOOL(RACTuple *value) {
        NSNumber *changeTypeValue = value.fourth;
        return changeTypeValue.integerValue == NSFetchedResultsChangeInsert;
    }] map:^id(RACTuple *value) {
        @strongify(self);
        return [self indexPathFromTuple:value];
    }];
    
    self.messageDeleteSignal = [[self.fetchedResultsControllerSignal filter:^BOOL(RACTuple *value) {
        NSNumber *changeTypeValue = value.fourth;
        return changeTypeValue.integerValue == NSFetchedResultsChangeDelete;
    }] map:^id(RACTuple *value) {
        @strongify(self);
        return [self indexPathFromTuple:value];
    }];
}

- (void) deleteMessage:(TCMessage *)message {
    
}


- (void) sendImageMessage:(UIImage *)image
                  withURL:(NSURL *)assetURL {
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0);
    
    dispatch_async(queue, ^{
        
        
        NSString *path = [assetURL absoluteString];
        NSArray *components = [path componentsSeparatedByString:@"id="];
        NSString *fileName = nil;
        if (components.count >= 1) {
            fileName = components[1];
            fileName = [fileName stringByReplacingOccurrencesOfString:@"&ext=JPG" withString:@""];
        }
        
        if (!fileName) {
            return;
        }
        
        fileName = [fileName stringByAppendingPathExtension:@"JPG"];
        
        NSString *tnumbnailName = [[fileName stringByAppendingString:thumbnailURLSuffixKey] stringByAppendingPathExtension:@"JPG"];
        
        NSURL *bigImageURL = [[NSFileManager libraryDirectoryURL] URLByAppendingPathComponent:fileName];
        
        NSURL *thumbnailURL = [[NSFileManager libraryDirectoryURL] URLByAppendingPathComponent:tnumbnailName];
        
        BOOL isBigFileExist = [[NSFileManager defaultManager] fileExistsAtPath:bigImageURL.path];
        
        BOOL isThumbnailExist = [[NSFileManager defaultManager] fileExistsAtPath:thumbnailURL.path];
        
        
        if (!isBigFileExist) {
            isBigFileExist = [UIImageJPEGRepresentation(image, 1.) writeToURL:bigImageURL
                                                                   atomically:YES];
        }
        
        if (!isThumbnailExist) {
            
            CGSize tumbnailSize = [self scaleSize:image.size withMaxSize: CGSizeMake(200, 200)];
            
            @autoreleasepool {
                isThumbnailExist = [self createThumbnailForImage:image
                                                      writeToURL:thumbnailURL
                                                       usingSize:tumbnailSize];
            }
            
        }
        
        NSAssert((isBigFileExist && isThumbnailExist), @"Image not saved");
        
        
        NSDictionary *info = @{mediaBigImageURLKey : fileName,
                               mediaThumbnailImageURLKey : tnumbnailName,
                               mediaTypeKey : @(kMediaTypePhoto)};
        
        [self.messageSender sendMessageWithText:@"Image was sent"
                                      imageInfo:info
                                    andLocation:nil];
        
    });
}

- (void) sendMessageWithLatitude:(NSNumber *)latitude
                       longitude:(NSNumber *)longitude
                 andPreviewImage:(UIImage *)image {
    
    if (!latitude || !longitude) {
        return;
    }
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0);
    
    dispatch_async(queue, ^{
        
        NSString *name = [[NSString stringWithFormat:@"%@_%@", latitude, longitude] stringByAppendingPathExtension:@"JPG"];
        NSURL *locationPreviewURL = [[NSFileManager libraryDirectoryURL] URLByAppendingPathComponent:name];
        
        BOOL isPreviewExist = [[NSFileManager defaultManager] fileExistsAtPath:locationPreviewURL.path];
        
        if (!isPreviewExist) {
            isPreviewExist = [UIImageJPEGRepresentation(image, 1.) writeToURL:locationPreviewURL
                                                                   atomically:YES];
        }
    
        NSDictionary *location = @{ locationPreviewKey : name,
                                    locationLatitudeKey : latitude,
                                    locationlongitudeKey : longitude};
        
        [self.messageSender sendMessageWithText:nil
                                      imageInfo:nil
                                    andLocation:location];
    });
}

- (void) sendTextMessage:(NSString *)messageText {
    
    [self.messageSender sendMessageWithText:messageText
                                  imageInfo:nil
                                andLocation:nil];
}

#pragma mark - Utils

- (CGSize) scaleSize:(CGSize)size
         withMaxSize:(CGSize)maxSize {
    
    CGRect originalFrame = CGRectMake(0, 0, size.width, size.height);
    CGRect maxFrame = CGRectMake(0, 0, maxSize.width, maxSize.height);
    CGFloat scale = 0;
    
    if (CGRectContainsRect(originalFrame, maxFrame)) {
        
        scale = MIN(maxSize.width / size.width,
                    maxSize.height / size.height);
    } else {
        
        scale = MIN(size.width / maxSize.width,
                    size.height / maxSize.height);
    }
    
    size.width *= scale;
    size.height *= scale;
    return size;
}

- (BOOL) createThumbnailForImage:(UIImage *)bigImage
                      writeToURL:(NSURL *)url
                       usingSize:(CGSize)tumbnailSize {
    
    BOOL isThumbnailExist = NO;
    
    UIGraphicsBeginImageContextWithOptions(tumbnailSize, NO, 0.0);
    [bigImage drawInRect:CGRectMake(0, 0, tumbnailSize.width, tumbnailSize.height)];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    isThumbnailExist = [UIImageJPEGRepresentation(image, 1.) writeToURL:url
                                                             atomically:YES];
    return isThumbnailExist;
}
@end
