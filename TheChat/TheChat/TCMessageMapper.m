//
//  TCMessageMapper.m
//  TheChat
//
//  Created by Chinara on 6/25/16.
//  Copyright © 2016 Chinara Kuzekeeva. All rights reserved.
//

#import "TCMessageMapper.h"
#import <FastEasyMapping/FastEasyMapping.h>
#import "TCMessage.h"
#import "TCUser.h"
#import "TCDBConstants.h"
#import "TCChat.h"
#import "TCMedia.h"

@interface TCMessageMapper () {
    
}
@property (nonatomic, strong) NSMutableArray *currentRelationShips;
@end

@implementation TCMessageMapper

- (NSEntityDescription *) descriptionForClass:(Class)class
                                    inContext:(NSManagedObjectContext *) context {
    
    NSEntityDescription *descr =
    [NSEntityDescription entityForName:NSStringFromClass(class)
                inManagedObjectContext:context];
    
    return descr;
}

+ (FEMMapping *) entityMapping:(NSEntityDescription *)description
         withParentDescription:(NSEntityDescription *)parentEntity {
    
    NSString *className = description.name;
    
    FEMMapping *mapping = [[FEMMapping alloc] initWithEntityName:className];
    
    NSDictionary *attributes = [description attributesByName];
    
    id obj = @"objectId";
    
    [mapping setPrimaryKey:obj];
    
    if (attributes.count > 0) {
        [mapping addAttributesFromArray:attributes.allKeys];
    }
    NSDictionary *relations = description.relationshipsByName;
    
    for (NSString *key in relations.allKeys) {
        
        NSRelationshipDescription *relationshipDescription = relations[key];
        
        NSString *destName = relationshipDescription.destinationEntity.name;
        
        if (![destName isEqualToString:parentEntity.name]) {
            
            NSEntityDescription *entityDescription = relationshipDescription.destinationEntity;
            
            FEMManagedObjectMapping *relationshipMapping = [self entityMapping:entityDescription
                                                         withParentDescription:description];
            
            if (relationshipDescription.toMany) {
                [mapping addToManyRelationshipMapping:relationshipMapping
                                          forProperty:key
                                              keyPath:key];
                
            } else {
                
                [mapping addRelationshipMapping:relationshipMapping
                                    forProperty:key
                                        keyPath:key];
            }
        }
    }
    return mapping;
}

+ (NSManagedObject *) objectFromEntityWithName:(NSString *)name
                              parentEntityName:(NSString *)parentName
                                fromDictionary:(NSDictionary *)dictionary
                                   withContext:(NSManagedObjectContext *)context {
    
    NSEntityDescription *messageDescription = [NSEntityDescription entityForName:name
                                               
                                                          inManagedObjectContext:context];
    
    FEMMapping *mapping = [self entityMapping:messageDescription
                        withParentDescription:nil];
    
    FEMDeserializer *deserializer = [[FEMDeserializer alloc] initWithContext:context];
    NSManagedObject *object = [deserializer objectFromRepresentation:dictionary
                                                             mapping:mapping];
    
    return object;
    
}

- (TCMessage *) messageFromDictionary:(NSDictionary *)dictionary
                          withContext:(NSManagedObjectContext *)context{
    
    id message = [TCMessageMapper objectFromEntityWithName:[TCMessage entityName] parentEntityName:nil                                  fromDictionary:dictionary
                                               withContext:context];
    
    NSAssert([message isKindOfClass:[TCMessage class]], @"TCMessage class expected");
    return message;
}

- (TCUser *) userFromDictionary:(NSDictionary *)dictionary
                    withContext:(NSManagedObjectContext *)context {
    
    id user = [TCMessageMapper objectFromEntityWithName:[TCUser entityName]
                                       parentEntityName:nil
                                         fromDictionary:dictionary
                                            withContext:context];
    
    NSAssert([user isKindOfClass:[TCUser class]], @"TCUser class expected");
    return user;
}

- (TCChat *) chatFromDictionary:(NSDictionary *)dictionary
                    withContext:(NSManagedObjectContext *)context {
    
    id chat = [TCMessageMapper objectFromEntityWithName:[TCChat entityName]
                                       parentEntityName:nil
                                         fromDictionary:dictionary
                                            withContext:context];
    
    NSAssert([chat isKindOfClass:[TCChat class]], @"TCChat class expected");
    return chat;
    
}

+ (NSDictionary *) serializeEntity:(id)managedObject
                          withName:(NSString *)name
                         inContext:(NSManagedObjectContext *)context {
    
    NSEntityDescription *description = [NSEntityDescription entityForName:name
                                        
                                                   inManagedObjectContext:context];
    
    FEMMapping *mapping = [self entityMapping:description
                        withParentDescription:nil];
    
    return [FEMSerializer serializeObject:managedObject
                             usingMapping:mapping];
}

+ (NSDictionary *) serializeUserEntityAttributes:(TCUser *)user {
    
    FEMMapping *mapping = [[FEMMapping alloc] initWithEntityName:[TCUser entityName]];
    [mapping addAttributesFromArray:@[objectIdKey, userNameKey]];
    
    return [FEMSerializer serializeObject:user
                             usingMapping:mapping];
}

@end
