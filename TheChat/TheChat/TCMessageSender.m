//
//  TCMessageSender.m
//  TheChat
//
//  Created by Chinara on 6/27/16.
//  Copyright © 2016 Chinara Kuzekeeva. All rights reserved.
//

#import "TCMessageSender.h"
#import "TCDBWrapper.h"
#import "TCDBConstants.h"
#import "TCUserModel.h"

#import <MapKit/MapKit.h>

@implementation TCMessageSender

- (void) sendMessageWithText:(NSString *)text
                   imageInfo:(NSDictionary *)imageInfo
                 andLocation:(NSDictionary *)location
                   forUserId:(NSNumber *)userId {
    
    NSDate *date = [NSDate date];
    
    NSUInteger components = NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay;
    
    NSDateComponents* comps = [[NSCalendar currentCalendar] components:components
                                                              fromDate:date];
    
    NSDate* dateOnly = [[[NSCalendar currentCalendar] dateFromComponents:comps] dateByAddingTimeInterval:[[NSTimeZone localTimeZone]secondsFromGMT]];
    
    NSMutableDictionary *message = [NSMutableDictionary dictionary];
    if(text) {
        message[messageTextKey] = text;
    }
    message[messageDayOfDate] = dateOnly;
    message[messageDateCreatedKey] = date;
    message[@"userId"] = userId;
    message[@"chat"] = @{objectIdKey :  @(systemChatId)};
    
    if (location) {
        message[messageLocationKey] = location;
    }
    
    if (imageInfo) {
        message[messageMediasKey] = imageInfo;
    }
    
    [[TCDBWrapper instance] createMessageWithDictionary:message];
}

- (void) sendMessageWithText:(NSString *)text
                   imageInfo:(NSDictionary *)imageInfo
                 andLocation:(NSDictionary *)location {
    
    NSNumber *systemUserId = [TCUserModel instanse].currentUserId;
    
    [self sendMessageWithText:text
                    imageInfo:imageInfo
                  andLocation:location
                    forUserId:systemUserId];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1. * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        NSNumber *botUserId = [TCUserModel instanse].botUserId;
        [self sendMessageWithText:text
                        imageInfo:imageInfo
                      andLocation:location
                        forUserId:botUserId];
    });
}

@end
