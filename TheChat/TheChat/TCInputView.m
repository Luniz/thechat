//
//  TCInputView.m
//  TheChat
//
//  Created by Chinara on 6/25/16.
//  Copyright © 2016 Chinara Kuzekeeva. All rights reserved.
//

#import "TCInputView.h"

static CGSize const sendButtonSize = {40.f , 40.f};
static CGSize const attachmentButtonSize = {40.f , 40.f};
static CGFloat const gapBetweenViews = 4.f;

@interface TCInputView () <UITextFieldDelegate> {
    
}
@property (nonatomic, strong, readwrite) UITextView *textView;
@property (nonatomic, strong, readwrite) UIButton *attachmentButton;
@property (nonatomic, strong, readwrite) UIButton *sendButton;

@property (nonatomic, strong, readwrite) NSArray *attachments;

@property (nonatomic, strong, readwrite) RACSignal *attachmentSignal;
@property (nonatomic, strong, readwrite) RACSignal *sendSignal;

@property (nonatomic, strong) NSLayoutConstraint *height;
@end

@implementation TCInputView

- (instancetype) initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        
        self.textView = [[UITextView alloc] initWithFrame:CGRectZero];
        self.sendButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.attachmentButton = [UIButton buttonWithType:UIButtonTypeCustom];
        NSArray <UIView *>*views = @[self.textView, self.attachmentButton, self.sendButton];
        
        for (UIView *view in views) {
            [self addSubview:view];
            view.translatesAutoresizingMaskIntoConstraints = NO;
        }
        
        [self bindActions];
        [self adjustViews];
        [self adjustConstraints];
    }
    
    return self;
}

- (void) bindActions {

    self.attachmentSignal = [[[self.attachmentButton rac_signalForControlEvents:UIControlEventTouchUpInside] takeUntil:self.rac_willDeallocSignal] deliverOnMainThread];
    
    self.sendSignal = [[[self.sendButton rac_signalForControlEvents:UIControlEventTouchUpInside] takeUntil:self.rac_willDeallocSignal] deliverOnMainThread];
}

- (void) adjustViews {
    
    [self.attachmentButton setImage:[UIImage imageNamed:@"attachment"]
                           forState:UIControlStateNormal];
    
    [self.sendButton setImage:[UIImage imageNamed:@"send"]
                     forState:UIControlStateNormal];
}

- (void) adjustConstraints {
    
    NSDictionary *views = @{@"attachmentButton" : self.attachmentButton,
                            @"sendButton" : self.sendButton,
                            @"textField" : self.textView};
    
    NSDictionary *metrics = @{@"gap" : @(gapBetweenViews),
                              @"leftMargin" : @(0),
                              @"rightMargin" : @(0),
                              @"sendButtonWidth" : @(sendButtonSize.width),
                              @"attachmentButtonWidth" : @(attachmentButtonSize.width)};
    
    NSArray *horisontal = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(leftMargin)-[attachmentButton(attachmentButtonWidth)]-(gap)-[textField]-(gap)-[sendButton(sendButtonWidth)]-(rightMargin)-|" options:0 metrics:metrics views:views];
    
    NSLayoutConstraint *centerAttachment = [NSLayoutConstraint constraintWithItem:self.attachmentButton attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterY multiplier:1. constant:0];
    
    NSLayoutConstraint *centerSend = [NSLayoutConstraint constraintWithItem:self.sendButton attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterY multiplier:1. constant:0];
    
    NSLayoutConstraint *heightAttachment =  [NSLayoutConstraint constraintWithItem:self.attachmentButton attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:self.attachmentButton attribute:NSLayoutAttributeWidth multiplier:1. constant:0];
    
    NSLayoutConstraint *heightSend =  [NSLayoutConstraint constraintWithItem:self.sendButton attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:self.sendButton attribute:NSLayoutAttributeWidth multiplier:1. constant:0];
    
    NSArray *verticalTextField = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[textField]-0-|" options:NSLayoutFormatAlignAllTop metrics:metrics views:views];
    
    horisontal = [[horisontal arrayByAddingObjectsFromArray:verticalTextField] arrayByAddingObjectsFromArray:@[centerAttachment, centerSend, heightSend, heightAttachment]];
    
    self.height = [NSLayoutConstraint constraintWithItem:self attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:0 multiplier:1. constant:CGRectGetHeight(self.bounds)];
    
    [self addConstraints:horisontal];
    [self addConstraint:self.height];
}

- (BOOL) isFirstResponder {
    return [self.textView isFirstResponder];
}

- (BOOL) resignFirstResponder {

    if ([self.textView isFirstResponder]) {
        return [self.textView resignFirstResponder];
    }
    return [super resignFirstResponder];
}

- (NSString *) messageText {
    return [self.textView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

- (void) clearInputField {
    self.textView.text = nil;
}
@end
