//
//  TCChatModel.h
//  TheChat
//
//  Created by Chinara on 6/25/16.
//  Copyright © 2016 Chinara Kuzekeeva. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TCChatModelProtocol.h"

@protocol TCMessageLifeCircleDelegate <NSObject>

@end

@interface TCChatModel : NSObject <TCChatModelProtocol>



@end
