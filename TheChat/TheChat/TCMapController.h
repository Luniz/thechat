//
//  TCMapController.h
//  TheChat
//
//  Created by Chinara on 6/27/16.
//  Copyright © 2016 Chinara Kuzekeeva. All rights reserved.
//

#import <UIKit/UIKit.h>
@class RACBehaviorSubject;
@interface TCMapController : UIViewController

@property (nonatomic, strong) RACBehaviorSubject *locationSelectionSignal;
@end
