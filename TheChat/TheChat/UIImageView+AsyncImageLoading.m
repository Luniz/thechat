//
//  UIImageView+AsyncImageLoading.m
//  TheChat
//
//  Created by Chinara on 6/26/16.
//  Copyright © 2016 Chinara Kuzekeeva. All rights reserved.
//

#import "UIImageView+AsyncImageLoading.h"
#import "NSFileManager+TheChat.h"

@implementation UIImageView (AsyncImageLoading)

- (void) loadImageFromPath:(NSString *)path
              placeHolder:(UIImage *)placeholder {
    
    self.image = placeholder;
    
    if (!path) {
        return;
    }
    
    NSURL *url = [[NSFileManager libraryDirectoryURL] URLByAppendingPathComponent:path];
    
    NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (data) {
            UIImage *image = [UIImage imageWithData:data];
            if (image) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.image = image;
                });
            }
        }
    }];
    [task resume];
}
@end
