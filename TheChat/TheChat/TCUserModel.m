//
//  TCUserModel.m
//  TheChat
//
//  Created by Chinara on 6/25/16.
//  Copyright © 2016 Chinara Kuzekeeva. All rights reserved.
//

#import "TCUserModel.h"
#import "TCDBWrapper.h"
#import "TCDBConstants.h"

@implementation TCUserModel

+ (instancetype) instanse {
    
    static TCUserModel *userModel;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        userModel = [TCUserModel new];
        [userModel commonInit];
    });
    return userModel;
}

- (void) commonInit {
    
    NSDictionary *systemUserInfo = @{ objectIdKey : @(systemUserId),
                                      userNameKey : @"DR. Jekyll"};
    
    NSDictionary *botUserInfo = @{ objectIdKey : @(botUserId),
                                   userNameKey : @"MR. Hyde"};
    
    TCUser *systemUser = [[TCDBWrapper instance] userWithDictionary:systemUserInfo];
    TCUser *botUser = [[TCDBWrapper instance] userWithDictionary:botUserInfo];
    self.currentUserId = systemUser.objectId.copy;
    self.botUserId = botUser.objectId.copy;
}
@end
