//
//  TCMessageSender.h
//  TheChat
//
//  Created by Chinara on 6/27/16.
//  Copyright © 2016 Chinara Kuzekeeva. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CLLocation;

@interface TCMessageSender : NSObject

- (void) sendMessageWithText:(NSString *)text
                   imageInfo:(NSDictionary *)imageInfo
                 andLocation:(NSDictionary *)location;
@end
