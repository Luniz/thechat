//
//  TCGropsListController.m
//  TheChat
//
//  Created by Chinara on 6/25/16.
//  Copyright © 2016 Chinara Kuzekeeva. All rights reserved.
//

#import "TCGroupsListController.h"
#import "TCChatModel.h"
#import "TCGroupsListModel.h"
#import "TCChat.h"

@interface TCGroupsListController ()

@property (weak, nonatomic) IBOutlet UIButton *chatButton;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *actIndicator;
@property (nonatomic, strong) TCGroupsListModel *groupModel;
@end

@implementation TCGroupsListController 

- (instancetype) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.groupModel = [TCGroupsListModel new];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    @weakify(self);
    
    [self.actIndicator startAnimating];
    self.chatButton.enabled = NO;
    
    [[[self.groupModel createDefaultChatEntity] takeUntil:self.rac_willDeallocSignal] subscribeNext:^(id x) {
        @strongify(self);
        
        [self.actIndicator stopAnimating];
        self.chatButton.enabled = YES;
        NSString *name = [NSString stringWithFormat:@"Open chat: %@", self.groupModel.chatName];
        
        [self.chatButton setTitle:name
                         forState:UIControlStateNormal];
    }];
    
    [[self.chatButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        @strongify(self);
        [self performSegueWithIdentifier:@"showChatController"
                                  sender:nil];
    }];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue
                 sender:(nullable id)sender {
    
    if ([segue.identifier isEqualToString:@"showChatController"]) {
        
        TCChatModel *model = [TCChatModel modelWithChatId:self.groupModel.chatId];
        if ([segue.destinationViewController respondsToSelector:@selector(model)]) {
            [segue.destinationViewController setValue:model
                                               forKey:@"model"];
        }
    }
}
@end
