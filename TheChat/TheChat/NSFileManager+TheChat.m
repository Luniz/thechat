//
//  NSFileManager+TCFileManager.m
//  TheChat
//
//  Created by Chinara on 6/26/16.
//  Copyright © 2016 Chinara Kuzekeeva. All rights reserved.
//

#import "NSFileManager+TheChat.h"

@implementation NSFileManager (TheChat)


+ (NSURL *) libraryDirectoryURL {
    
    static NSURL *imageUrl;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        
        NSArray *directories = [[NSFileManager defaultManager] URLsForDirectory:NSLibraryDirectory
                                                                      inDomains:NSUserDomainMask];
        
        imageUrl = [directories firstObject];
        
    });
    return imageUrl;
}

@end
